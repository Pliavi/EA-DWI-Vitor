var gulp = require('gulp')
var pug = require('gulp-pug')
var stylus = require('gulp-stylus')
var pump = require('pump')

// convetendo os arquivos .pug para html
gulp.task('html', (cb) => {
  pump([
    gulp.src(['html/**/*.pug', '!html/include/*']),
    pug(),
    gulp.dest('__site'),
  ], cb)
})

// convertendo os arquivos .stylus pra .css
gulp.task('css', (cb) => {
  pump([
    gulp.src('assets/css/**/*.stylus'),
    stylus(),
    gulp.dest('__site/assets/css')
  ], cb)
})

// Copiando os arquivos javascript e imagens para as pasta da dist __site
gulp.task('copies', (cb) => {
  pump([
    gulp.src('assets/js/**/*.js'),
    gulp.dest('__site/assets/js'),

    gulp.src('assets/img/**/*'),
    gulp.dest('__site/assets/img'),

    gulp.src('assets/img/galeria/thumbs/*'),
    gulp.dest('__site/assets/img/galeria/thumbs')
  ], cb)
})

gulp.task('tasks', [ 'html', 'css', 'copies' ])
gulp.task('watch', () => gulp.watch([ 'html/**/*', 'assets/**/*' ] , ['tasks']))
gulp.task('default', [ 'tasks', 'watch' ])